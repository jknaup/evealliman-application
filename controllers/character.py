__author__ = 'Ruindur <ruindur@gmx.de>'


def characterApiQuery():
    form = SQLFORM.factory(Field('name', 'string', length=64, comment='Eve character name'))
    if form.process().accepted:
        charactername=form.vars.name
        characterinfo = _ingestCharacterByName(charactername)
        characterinfo['form']=form
        return characterinfo
    else:
        return dict(form=form)


def characterListApiQuery():
    form=SQLFORM.factory(Field('names', 'text', comment='One charcter name per line'))
    if form.process().accepted:
        characters = []
        charids = []
        for charactername in form.vars.names.split('\n'):
            cinfo=_ingestCharacterByName(charactername.strip())
            if cinfo['secstatus'] <= -5.0:
                color = 'red'
            elif cinfo['secstatus'] < -0.0:
                color = 'orange'
            elif cinfo['secstatus'] < +5.0:
                color = 'black'
            else:
                color = 'green'
            charids.append(cinfo['id'])
            characters.append(TR(TD(IMG(_src='https://image.eveonline.com/Character/{0:d}_64.jpg'.format(cinfo['id']),
                    _alt='character portrait'), XML('<br />'), cinfo['name']),
                        TD(cinfo['race'], ' ', cinfo['bloodline']),
                        TD(SPAN('{0:4.2f}'.format(cinfo['secstatus']), _style='color: {0:s}'.format(color))),
                        TD(cinfo['corporationname'])))
        chartable=TABLE(TR(TH('Character'), TH('Race'), TH('Security'), TH('Corporation')), *characters)
        return dict(form=form, characters=chartable, charids=charids)
    else:
        return dict(form=form)


@auth.requires_membership('intel')
def updateCharacters():
    eveCharacters = db(db.evecharacter.id > 0).select(db.evecharacter.id)
    for evechar in eveCharacters:
        _ingestCharacterById(int(evechar['id']))
    numemploymens = _calculateEmploymentEnds()['updatecount']
    #_calculateCorpmateships()
    return dict(numemploymens=numemploymens, numchars=len(eveCharacters))


def characterSummary():
    characterid = int(request.args[0])
    characterrow = db(db.evecharacter.id == characterid).select(db.evecharacter.name, db.evecharacter.ts,
                                                                db.evecorporation.name, db.evecorporation.ticker,
                                                                join=[db.evecorporation.on(
                                                                    db.evecharacter.corporationid == db.evecorporation.id
                                                                )])
    if len(characterrow) == 0:
        return dict(name='Character unknown', id=0, secstatus='N/A', secstatuschange='N/A',
                    corpname='N/A', corpticker='')
    secstatusrows = db(db.evecharacterSecstatus.evecharacter == characterid).select(
        db.evecharacterSecstatus.secstatus, limitby=(0, 2), orderby=db.evecharacterSecstatus.ts
    )
    if len(secstatusrows) == 2:
        difference = secstatusrows[-2]['secstatus']-secstatusrows[-1]['secstatus']
        if difference >= 0:
            secstatuschange = SPAN('{0:+4.2f}'.format(difference), _style='color: green')
        else:
            secstatuschange = SPAN('{0:+4.2f}'.format(difference), _style='color: red')
    else:
        secstatuschange = 'N/A'
    if len(secstatusrows) > 0:
        secstatus = secstatusrows[-1]['secstatus']
        if secstatus < -5.0:
            securtiy = SPAN('{0:4.2f}'.format(secstatus), _style="color: red")
        elif secstatus < 0.0:
             securtiy = SPAN('{0:4.2f}'.format(secstatus), _style="color: orange")
        elif secstatus >= 0.0:
             securtiy = SPAN('{0:4.2f}'.format(secstatus), _style="color: black")
        else:
             securtiy = SPAN('{0:4.2f}'.format(secstatus), _style="color: black")
    else:
        securtiy = 'N/A'
    return dict(name=characterrow[0]['evecharacter.name'],
                corpname=characterrow[0]['evecorporation.name'],
                corpticker=characterrow[0]['evecorporation.ticker'],
                secstatus=securtiy,
                secstatuschange=secstatuschange,
                id=characterid
                )
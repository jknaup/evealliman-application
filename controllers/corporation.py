import json
import urllib2
import datetime
import evelink

__author__ = 'Ruindur <ruindur@gmx.de>'


def _ingestCharacterById(charid):
    eve = evelink.eve.EVE()
    if charid is None:
        characterinfo = dict(name='Character not found', race='', bloodline='', secstatus=0.0, id=0)
    else:
        resp_info = eve.character_info_from_id(charid)
        result = resp_info.result
        charts = datetime.datetime.fromtimestamp(resp_info.timestamp)
        corpexists = db(db.evecorporation.id == result['corp']['id']).select(db.evecorporation.id)
        if len(corpexists) == 0:
            _updateCorpFromId(result['corp']['id'])
        db.evecharacter.update_or_insert(db.evecharacter.id == charid, id=charid, name=result['name'],
                                         race=result['race'],
                                         bloodline=result['bloodline'], corporationid=result['corp']['id'],
                                         ts=charts)
        db.evecharacterSecstatus.insert(evecharacter=charid, secstatus=result['sec_status'], ts=charts)
        for employ in result['history']:
            startts = datetime.datetime.fromtimestamp(employ['start_ts'])
            emrows = db((db.evecharacterEmployment.evecharacter == charid) &
                        (db.evecharacterEmployment.joints == startts)).select(db.evecharacterEmployment.id)
            if not emrows:
                db.evecharacterEmployment.insert(evecharacter=charid, joints=startts,
                                                 corporationid=employ['corp_id'])
        characterinfo = dict(name=result['name'], race=result['race'], bloodline=result['bloodline'],
                             secstatus=result['sec_status'], id=charid, corporationname=result['corp']['name'])
    return characterinfo


def _ingestCharacterByName(charactername):
    eve = evelink.eve.EVE()
    charid = eve.character_id_from_name(charactername).result
    characterinfo = _ingestCharacterById(charid)
    return characterinfo


@auth.requires_membership('intel')
def initializeCharacterCorps():
    corporationids = db(db.evecorporation.id > 0)._select(db.evecorporation.id)
    ccrows = db(not (db.evecharacter.corporationid.belongs(corporationids))).select(db.evecharacter.corporationid)
    return dict(rows=SQLTABLE(ccrows), rowcount=len(ccrows), qqq=ccrows, query=db._lastsql)


@auth.requires_membership('intel')
def initializeEmploymentCorps():
    corporationids = db(db.evecorporation.id > 0)._select(db.evecorporation.id)
    ccrows = db(not (db.evecharacterEmployment.corporationid.belongs(corporationids))).select(
        db.evecharacterEmployment.corporationid, distinct=True)
    for corp in ccrows:
        _updateCorpFromId(corp.corporationid)
    return dict(rows=SQLTABLE(ccrows))


@auth.requires_membership('intel')
def memberlist():
    try:
        corpid = request.args[0]
    except IndexError:
        return dict(members=[])
    memberlines = db(db.evecharacter.corporationid == corpid).select(db.evecharacter.id, db.evecharacter.name)
    members=[]
    for memberline in memberlines:
        members.append(dict(name=memberline['name'], id=memberline['id']))
    return dict(members=members)


@auth.requires_membership('intel')
def corpdetails():
    try:
        corpid = int(request.args[0])
    except IndexError:
        return dict(corpname='Corporation not found')
    _updateCorpFromId(corpid)
    corpentryline = db(db.evecorporation.id == corpid).select(db.evecorporation.ALL)
    memberemploymentcountline = db(db.evecharacter.corporationid == corpid).select(db.evecharacter.corporationid,
                                                                                   db.evecharacter.id.count(),
                                                                                   groupby=db.evecharacter.corporationid,
                                                                                   )
    if len(memberemploymentcountline)>0:
        knownmembercount = memberemploymentcountline[0]['_extra']['COUNT(evecharacter.id)']
    else:
        knownmembercount = 0
    if len(corpentryline) == 0:
        return dict(corpname='Corporation not found')
    return dict(corpname=corpentryline[0]['name'], corpid=corpid, corpticker=corpentryline[0]['ticker'],
                knownmembercount=knownmembercount)


def _fetchCorpmembersFromEvewho(corpid):
    charactersleft=True
    page=0
    charcount=0
    while charactersleft:
        try:
            ewreply=urllib2.urlopen('http://evewho.com/api.php?type=corplist&id={0:d}&page={1:d}'.format(corpid,page)).read()
        except URLError:
            return dict(corpname=ewdata['error'], charactercount=0)
        ewdata=json.loads(ewreply)
        if "error" in ewdata:
            response.flash = 'Data unavailable for requested corporation.'
            return dict(corpname=ewdata['error'], charactercount=0)
        if len(ewdata['characters']) >= 200:
            page += 1
        else:
            charactersleft = False
        for character in ewdata['characters']:
            _ingestCharacterById(character['character_id'])
            charcount += 1
    return dict(corpname=ewdata['info']['name'], charactercount=charcount)


def _fetchCorpmatesFromEvewho(charid):
    corprows = db(db.evecharacter.id == charid).select(db.evecharacter.corporation)
    if corprows[-1].corporation is not None:
        return _fetchCorpmembersFromEvewho(corprows[-1].corporation)
    else:
        return dict(corpname='Unknown', charactercount=0)


@auth.requires_membership('intel')
def fillCorpMembers():
    form=SQLFORM.factory(Field('corporation', requires=IS_IN_DB(db, 'evecorporation.id', '%(name)s')))
    if form.process().accepted:
        fillresult=_fetchCorpmembersFromEvewho(int(form.vars.corporation))
        return dict(corpname=fillresult['corpname'], charactercount=fillresult['charactercount'], form=form)
    else:
        return dict(form=form)


@auth.requires_membership('intel')
def viewcorporation():
    form=SQLFORM.factory(Field('corporation', requires=IS_IN_DB(db, 'evecorporation.id', '%(name)s')))
    if form.process().accepted:
        return dict(corpid=form.vars.corporation, form=form)
    else:
        return dict(form=form)
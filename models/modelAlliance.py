import datetime

__author__ = 'Ruindur <ruindur@gmx.de>'

import evelink


db.define_table('evealliance',
                Field('name', 'string', length=80, comment='EVE alliance Name'),
                Field('ticker', 'string', length=10, comment='EVE alliance ticker'),
                migrate=True, redefine=True
)

db.define_table('evecorporation',
                Field('name', 'string', comment='EVE corporation name', writable=False, notnull=True),
                Field('ticker', 'string', comment='EVE corporation ticker', writable=False, notnull=True),
                migrate=True, redefine=True
)

db.define_table('evecharacter',
                Field('name', 'string', comment='character name', writable=False),
                Field('race', 'string', comment='race', writable=False),
                Field('bloodline', 'string', comment='bloodline', writable=False),
                Field('corporationid', 'reference evecorporation', comment='ID of current corporation', writable=False),
                Field('ts', 'datetime', comment='Last update', writable=False),
                Field('ownremark', 'text', comment='Self description'),
                migrate=True, redefine=True
)

db.define_table('evecharacterEmployment',
                Field('evecharacter', 'reference evecharacter', ondelete='cascade', comment='EVE character id',
                      writable=False, notnull=True),
                Field('corporationid', 'integer', notnull=True, writable=False, comment='ID of former corporation'),
                Field('joints', 'datetime', comment='Date and time corporation was joined', writable=False,
                      notnull=True),
                Field('leavets', 'datetime', comment='Date and time corporation was left', writable=False,
                      notnull=False),
                migrate=True, redefine=True
)

db.define_table('evecharacterLocation',
                Field('evecharacter', 'reference evecharacter', ondelete='cascade', comment='EVE character id',
                      writable=False, notnull=True),
                Field('evelocation', 'string', comment='EVE location name', notnull=True, writable=False),
                Field('ts', 'datetime', comment='Last update', writable=False),
                migrate=True, redefine=True
)

db.define_table('evecharacterSecstatus',
                Field('evecharacter', 'reference evecharacter', ondelete='cascade', comment='EVE character id',
                      writable=False, notnull=True),
                Field('secstatus', 'double', comment='Security status', writable=False, notnull=True),
                Field('ts', 'datetime', comment='Last update', writable=False),
                migrate=True, redefine=True
)

db.define_table('evecharacterShip',
                Field('evecharacter', 'reference evecharacter', ondelete='cascade', comment='EVE character id',
                      writable=False, notnull=True),
                Field('shiptypeid', 'integer', comment='EVE id of flown ship type', writable=False, notnull=True),
                Field('shiptypename', 'string', comment='EVE name of flown ship type', writable=False, notnull=True),
                Field('ts', 'datetime', comment='Last update', writable=False),
                migrate=True, redefine=True
)

db.define_table('evecharacterAccountbalance',
                Field('evecharacter', 'reference evecharacter', ondelete='cascade', comment='EVE character id',
                      writable=False, notnull=True),
                Field('accountbalance', 'double', comment='EVE character account balance', writable=False,
                      notnull=True),
                Field('ts', 'datetime', comment='Last update', writable=False),
                migrate=True, redefine=True
)

db.define_table('evecharacterCorpmateship',
                Field('evecharactera', 'reference evecharacter', ondelete='cascade', comment='Mate A', writable=False,
                      notnull=True),
                Field('evecharacterb', 'reference evecharacter', ondelete='cascade', comment='Mate B', writable=False,
                      notnull=True),
                Field('evecorporation', 'reference evecorporation', ondelete='cascade',
                      comment='Common corporation of A and B', writable=False, notnull=True),
                Field('startts', 'datetime', comment='Start of mate relation', writable=False, notnull=True),
                Field('endts', 'datetime', comment='End of mate relation', writable=False, notnull=False),
                redefine=True,
                fake_migrate=True
)

db.define_table('evecharacterWalletjournal',
                Field('ts', 'datetime', comment='Transaction time stamp', writable=False),
                Field('typeid', 'integer', comment='Transaction type', writable=False),
                Field('party1type', 'integer', comment='Type of first party', writable=False),
                Field('party1id', 'integer', comment='ID of first party', writable=False),
                Field('party1name', 'string', comment='Name of first party', writable=False),
                Field('party2type', 'integer', comment='Type of second party', writable=False),
                Field('party2id', 'integer', comment='ID of second party', writable=False),
                Field('party2name', 'string', comment='Name of second party', writable=False),
                Field('taxerid', 'integer', comment='ID of taxing party', writable=False),
                Field('taxamount', 'float', comment='amount of tax', writable=False),
                Field('amount', 'float', comment='Transaction amount', writable=False),
                Field('balance', 'float', comment='Balance after transaction', writable=False),
                Field('reason', 'string', comment='Transaction reason', writable=False),
                Field('argid', 'integer', comment='Argument ID', writable=False),
                Field('argname', 'string', comment='Argument name', writable=False)
                )

def _updateCorpFromId(corporationid):
    api = evelink.api.API()
    crp = evelink.corp.Corp(api)
    resp_corp = crp.corporation_sheet(corporationid)
    db.evecorporation.update_or_insert(db.evecorporation.id == corporationid, name=resp_corp.result['name'],
                                       ticker=resp_corp.result['ticker'], id=corporationid)


def _calculateEmploymentEnds():
    characterrows = db(db.evecharacter.id > 0).select(db.evecharacter.id)
    updatecount = 0
    for characterrow in characterrows:
        characterid = characterrow['id']
        employmentrows = db(db.evecharacterEmployment.evecharacter == characterid).select(db.evecharacterEmployment.ALL,
                                                                                          orderby='joints')
        for employindex in xrange(len(employmentrows) - 1):
            employment = employmentrows[employindex]
            nextemployment = employmentrows[employindex + 1]
            if employment['leavets'] is None:
                if nextemployment['joints'] is not None:
                    employment.update_record(leavets=nextemployment['joints'])
                    updatecount += 1
    return dict(updatecount=updatecount)


def _getCharacterCorpmates(characterid):
    corpmates = []
    characterHistoryCorps = db(db.evecharacterEmployment.evecharacter ==
                               characterid)._select(db.evecharacterEmployment.corporationid,
                                                    orderby=db.evecharacterEmployment.joints)
    cmrows = db(db.evecharacterEmployment.corporationid.belongs(characterHistoryCorps) &
                (db.evecharacterEmployment.evecharacter != characterid)).select(
        db.evecharacterEmployment.ALL, orderby=[db.evecharacterEmployment.evecharacter,
                                                db.evecharacterEmployment.joints]
    )


def _ingestCharacterById(charid):
    eve = evelink.eve.EVE()
    if charid is None:
        characterinfo = dict(name='Character not found', race='', bloodline='', secstatus=0.0, id=0)
    else:
        resp_info = eve.character_info_from_id(charid)
        result = resp_info.result
        charts = datetime.datetime.fromtimestamp(resp_info.timestamp)
        corpexists = db(db.evecorporation.id == result['corp']['id']).select(db.evecorporation.id)
        if len(corpexists) == 0:
            _updateCorpFromId(result['corp']['id'])
        db.evecharacter.update_or_insert(db.evecharacter.id == charid, id=charid, name=result['name'],
                                         race=result['race'],
                                         bloodline=result['bloodline'], corporationid=result['corp']['id'],
                                         ts=charts)
        db.evecharacterSecstatus.insert(evecharacter=charid, secstatus=result['sec_status'], ts=charts)
        for employ in result['history']:
            startts = datetime.datetime.fromtimestamp(employ['start_ts'])
            emrows = db((db.evecharacterEmployment.evecharacter == charid) &
                        (db.evecharacterEmployment.joints == startts)).select(db.evecharacterEmployment.id)
            if not emrows:
                db.evecharacterEmployment.insert(evecharacter=charid, joints=startts,
                                                 corporationid=employ['corp_id'])
        characterinfo = dict(name=result['name'], race=result['race'], bloodline=result['bloodline'],
                             secstatus=result['sec_status'], id=charid, corporationname=result['corp']['name'])
    return characterinfo


def _ingestCharacterByName(charactername):
    eve = evelink.eve.EVE()
    charid = eve.character_id_from_name(charactername).result
    characterinfo = _ingestCharacterById(charid)
    return characterinfo


def _ingestCharacterWallet(characterid, keyid, keycode):
    api = evelink.api.API(api_key=(int(keyid), keycode))
    characterapi = evelink.char.Char(char_id=characterid, api=api)
    maxentry = db(db.evecharacterWalletjournal.party1id == characterid).select(db.evecharacterWalletjournal.id.max())\
        .first()['MAX(evecharacterWalletjournal.id)']
    journalfinished = False
    journal = []
    pageid = None
    while not journalfinished:
        wjour = characterapi.wallet_journal(limit=2000, before_id=pageid).result
        journal += wjour
        pageid = min([ int(line['id']) for line in journal ])
        journalfinished = ((len(wjour) < 2000) or (pageid < maxentry))
    linecount = 0
    for line in journal:
        if int(line['id'])> maxentry:
            db.evecharacterWalletjournal.insert(id=line['id'], ts=datetime.datetime.fromtimestamp(line['timestamp']),
                                                party1id=line['party_1']['id'], party1type=line['party_1']['type'],
                                                party1name=line['party_1']['name'],
                                                party2id=line['party_2']['id'], party2type=line['party_2']['type'],
                                                party2name=line['party_2']['name'],
                                                amount=line['amount'], balance=line['balance'], reason=line['reason'],
                                                taxamount=line['tax']['amount'], taxerid=line['tax']['taxer_id'],
                                                typeid=line['type_id'],
                                                argname=line['arg']['name'], argid=line['arg']['id'])
            linecount += 1
    return linecount